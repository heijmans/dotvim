set ts=4
set sw=4
set ai
syntax on

execute pathogen#infect()
filetype plugin indent on

map t :CommandTBuffer<CR>
map T :CommandT<CR>

map gs :!git status<CR>
map gd :!git diff<CR>
map ga :!git add .<CR>

map gu :!git pull<CR>

map gc :!git commit -v<CR>
map gp :!git commit -v && git push<CR>
map gac :!git commit -av<CR>
map gap :!git commit -av && git push<CR>
